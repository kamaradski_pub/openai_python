# Davinci Codex API Python Script - README

## Description

This script is a simple implementation of the OpenAI API in Python. It allows users to submit a query to the OpenAI API and receive a response. The script takes the content of a file (prompt.txt) as natural language query and submits it to the OpenAI API using the requests library. The script then prints the reply of from the API.

## Requirements

- Python 3
- requests library (can be installed using `pip install requests`)
- json library (Python built-in library)
- os library (Python built-in library)
- An OpenAI API key (can be obtained from the OpenAI website)


## Usage

1) Set the `OPENAI_API_KEY` environment variable to your OpenAI API key.
2) Create a `prompt.txt` file in the same directory as the script and add the prompt you want the API to complete.
3) Run the script using `python script.py`


## Script Configuration

The script has several configuration options that can be adjusted to suit your needs. These options are located at the top of the script and are as follows:

- `api_key:` Your OpenAI API key.
- `endpoint:` The API endpoint URL.
- `prompt:` The text prompt that you want the API to complete.
- `temperature:` Controls the creativity of the API's response. A value of 0 will generate a response that is close to the training data, while a value of 1 will generate a more creative response.
- `max_tokens:` The maximum number of tokens (words) in the API's response.
- `top_p:` Determines the proportion of the mass of the API's response that comes from the top p tokens.
- `frequency_penalty:` Determines the degree to which the API will avoid repeating text from the input.
- `presence_penalty:` Determines the degree to which the API will avoid text that has been used recently.


## Output

The script will print the following output:

- Request settings: JSON object that contains the settings for the API request.
- Output body: The API's response to the prompt.

## Purpose

This script is designed to be used as a starting point for more complex projects that make use of the OpenAI API. It can be easily modified to suit different use cases and integrate with other systems.

## Contributions

- Pull requests are welcome
- If you have any suggestions or issues, please open them

## License

This code is open-sourced under the [MIT license](https://opensource.org/licenses/MIT). Please feel free to use, modify, and distribute the code as you wish.

## Final Note
Please keep in mind that the model used in this script is "davinci-codex" which is a language model that generates human-like text. OpenAI also provides other models that can be used for other tasks such as image generation, text-to-speech, etc. You can find more information about the models on OpenAI's website.
