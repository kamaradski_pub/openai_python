# Python implementation of: https://api.openai.com/v1/completions
# https://platform.openai.com/docs/api-reference/completions/create

import openai
import json
import os
import sys

openai.api_key = os.getenv("OPENAI_API_KEY")

# ==== Define the request parameters ====

# ID of the model to use.
# https://platform.openai.com/docs/models/overview
model_engine = "text-davinci-003"

# The prompt(s) to generate completions for, encoded as a string, array of strings, array of 
# tokens, or array of token arrays.
# Note that <|endoftext|> is the document separator that the model sees during training, so 
# if a prompt is not specified the model will generate as if from the beginning of a new document.
prompt = "Once upon a time "

# The maximum number of tokens to generate in the completion.
max_tokens = 150

# What sampling temperature to use, between 0 and 2. Higher values like 0.8 will make 
# the output more random, while lower values like 0.2 will make it more focused and deterministic.
# We generally recommend altering this or top_p but not both.
temperature = 0.5


# An alternative to sampling with temperature, called nucleus sampling, where the model considers the results of
# the tokens with top_p probability mass. So 0.1 means only the tokens comprising the top 10% probability 
# mass are considered.
# We generally recommend altering this or temperature but not both.
# top_p = 1

# How many completions to generate for each prompt.
# Note: Because this parameter generates many completions, it can quickly consume your token quota. 
# Use carefully and ensure that you have reasonable settings for max_tokens and stop.
n = 1

# ==== Call the API with streaming enabled ====
response = openai.Completion.create(
    engine=model_engine,
    prompt=prompt,
    max_tokens=max_tokens,
    temperature=temperature,
    #top_p=top_p,
    n=n,
)


# ==== output ====
#print(json.dumps(response, indent=2))
generated_text = response.choices[0].text.strip()
print(generated_text)