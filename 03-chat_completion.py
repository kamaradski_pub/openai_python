# Python implementation of: https://api.openai.com/v1/chat/completions
# https://platform.openai.com/docs/api-reference/chat/create

import openai
import json
import os

openai.api_key = os.getenv("OPENAI_API_KEY")

# ==== Define the request parameters ====

# ID of the model to use.
# https://platform.openai.com/docs/models/overview
model_engine = "gpt-3.5-turbo"


# The messages to generate chat completions for, in the chat format.
# https://platform.openai.com/docs/guides/chat/introduction
mymessage="""
once apon a time: 
"""

# The maximum number of tokens to generate in the completion.
max_tokens = 1024

# What sampling temperature to use, between 0 and 2. Higher values like 0.8 will make 
# the output more random, while lower values like 0.2 will make it more focused and deterministic.
# We generally recommend altering this or top_p but not both.
temperature = 0.5


# An alternative to sampling with temperature, called nucleus sampling, where the model considers the results of
# the tokens with top_p probability mass. So 0.1 means only the tokens comprising the top 10% probability 
# mass are considered.
# We generally recommend altering this or temperature but not both.
# top_p = 1

# How many completions to generate for each prompt.
# Note: Because this parameter generates many completions, it can quickly consume your token quota. 
# Use carefully and ensure that you have reasonable settings for max_tokens and stop.
n = 1

# ==== Call the API with streaming enabled ====
response = openai.ChatCompletion.create(
    engine=model_engine,
    messages=[{"role": "user", "content": mymessage}],
    max_tokens=max_tokens,
    #temperature=temperature,
    #top_p=top_p,
    #n=n,
)


# ==== output ====
print(completion.choices[0].message)
