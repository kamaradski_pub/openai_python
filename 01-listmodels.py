import os
import openai
import json


openai.api_key = os.getenv("OPENAI_API_KEY")
models = openai.Model.list()

# ==== option-1: print onlt the id (name) ====
for item in models["data"]:
    # Extract the "id" field from the dictionary
    current_id = item["id"]
    
    # Print the "id" field value
    print(current_id)

# # ==== option-2: print "id", "owned_by", "parent", "root" in new json ====
# # Create a list to store the output
# output = []

# # Loop through each dictionary in the "data" list
# for item in models["data"]:
#     # Extract the desired fields from the dictionary
#     current_id = item["id"]
#     owned_by = item["owned_by"]
#     parent = item["parent"]
#     root = item["root"]
    
#     # Create a new dictionary with the extracted fields
#     model_data = {
#         "id": current_id,
#         "owned_by": owned_by,
#         "parent": parent,
#         "root": root
#     }
    
#     # Add the new dictionary to the output list
#     output.append(model_data)

# # Convert the output list to JSON and print it
# output_json = json.dumps(output, indent=4)

# for item in output_json:
#   print(item["id"])