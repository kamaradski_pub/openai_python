import os
import openai


openai.api_key = os.getenv("OPENAI_API_KEY")


response = openai.Image.create(

  # A text description of the desired image(s). The maximum length is 1000 characters.
  prompt="something funny",
  
  # The number of images to generate. Must be between 1 and 10.
  n=2,

  # Must be one of 256x256, 512x512, or 1024x1024
  size="1024x1024"
)

print(response)